-- phpMyAdmin SQL Dump
-- version 3.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 22, 2020 at 09:51 AM
-- Server version: 5.1.30
-- PHP Version: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `maps_tc`
--

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `sno` int(200) NOT NULL AUTO_INCREMENT,
  `Fname` varchar(50) DEFAULT NULL,
  `login` varchar(50) DEFAULT NULL,
  `logout` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `login`
--


-- --------------------------------------------------------

--
-- Table structure for table `tc_data`
--

CREATE TABLE IF NOT EXISTS `tc_data` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `affiliation_no` bigint(20) DEFAULT NULL,
  `admin_no` bigint(20) DEFAULT NULL,
  `book_no` bigint(20) DEFAULT NULL,
  `column1` varchar(225) DEFAULT NULL,
  `column2` varchar(225) DEFAULT NULL,
  `column3` varchar(225) DEFAULT NULL,
  `column4` varchar(225) DEFAULT NULL,
  `column5` varchar(225) DEFAULT NULL,
  `column6` date DEFAULT NULL,
  `column7` date DEFAULT NULL,
  `column7_inwords` varchar(225) DEFAULT NULL,
  `column8` bigint(20) DEFAULT NULL,
  `column9` varchar(225) DEFAULT NULL,
  `column10` varchar(225) DEFAULT NULL,
  `column11` varchar(225) DEFAULT NULL,
  `column12` varchar(225) DEFAULT NULL,
  `column13` bigint(20) DEFAULT NULL,
  `column14` varchar(225) DEFAULT NULL,
  `column15` bigint(20) DEFAULT NULL,
  `column16` bigint(20) DEFAULT NULL,
  `column17` varchar(225) DEFAULT NULL,
  `column18` varchar(225) DEFAULT NULL,
  `column19` varchar(225) DEFAULT NULL,
  `column20` date DEFAULT NULL,
  `column21` date DEFAULT NULL,
  `column22` varchar(225) DEFAULT NULL,
  `column23` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tc_data`
--

